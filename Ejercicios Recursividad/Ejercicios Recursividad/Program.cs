﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Recursividad
{
    class Program
    {
        static void Main(string[] args)
        {
            Recurrencia r = new Recurrencia();
            int n = r.FibonacciR(8);
            int[] numbers = new int[] { 5, 4, 3, 2, 1 };
            Console.WriteLine(n);
            Console.WriteLine(r.Cadena("hola"));
            Console.WriteLine(r.Productoria(numbers, 4));
            Console.ReadKey(); 
        }
    }
}
