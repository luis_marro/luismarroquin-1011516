﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicios_Recursividad
{
    class Recurrencia
    {
        /*
            Con recurrencia: 
        1. Fibonacci 1 1 2 3 5 8 13 21 34 55 
        2. Inversión de cadena "Boris" --> "Sirob"
        3. Productoria de un array 
        4. G(x,y) { 1 / G(x-y, x + 1)   si x >= y 
                  { 2x - y              si x < y 
        */

        public int FibonacciR(int n)
        {
            if (n == 1)
            {
                return 0; 
            }
            else
            {
                if (n == 2)
                {
                    return 1;
                }
                return FibonacciR(n - 1) + FibonacciR(n - 2); 
            }
        }

        public string Cadena(string cadena)
        {
            if (cadena.Length == 1)
                return cadena;
            else
                //return cadena.Substring(0, 4); retorna los caracteres de 0 hasta 4 
                return (cadena[cadena.Length - 1] + Cadena(cadena.Substring(0, cadena.Length - 1)));  
        }

        public int Productoria(int[] numbers, int lenght)
        {
            if (lenght == 0)
                return numbers[lenght];
            else
                return Productoria(numbers, lenght--) * Productoria(numbers, lenght);   
        }

        public int Funcion(int x, int y)
        {
            if (x < y)
                return 2 * x - y;
            else
                return 1 / (Funcion(x - y, x + 1)); 
        }
    }
}
