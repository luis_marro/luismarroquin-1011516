﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estructuras_Básicas
{
    class Cola
    {
        public int[] elements;
        public int count; 

        public Cola(int size)
        {
            elements = new int[size];
            count = 0; 
        }

        public bool Queue(int e)
        {
            if(count < elements.Length)
            {
                elements[count++] = e;
                return true; 
            }
            return false; 
        }

        //Devuelve la primera posición 
        public int Deque()
        {
            if(count > 0)
            { 
                int t = elements[0]; 
                for(int i = 0; i < count; i++)
                {
                    elements[i] = elements[i + 1];  
                }
                count--;
                return t; 
            }
            return 0; 
        }
    }
}
