﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estructuras_Básicas
{
    class Program
    {
        static void Main(string[] args)
        {

            Lista myList = new Lista();
            myList.Initialize(10);
            myList.insertFIrst(40);
            myList.insertFIrst(20);
            myList.InsertLast(12); 
            Console.ReadLine();

            Cola myQueue = new Cola();
            myQueue.Initialize(5); 
            myQueue.Queue(2);
            myQueue.Queue(3);
            myQueue.Queue(4);
            myQueue.Deque();
            Console.ReadLine(); 
        }
    }
}
