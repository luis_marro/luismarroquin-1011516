﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Estructuras_Básicas
{
    public class Pila
    {
        public int[] elements;
        public int count;

        public Pila(int size)
        {
            elements = new int[size];
            count = 0;
        }

        public int Count
        {
            get { return count; }
        }

        public bool Push(int newElement)
        {
            if(count < elements.Length)
            { 
                 elements[count++] = newElement;
                return true; 
            }
            return false; 
        }

        public int Top()
        {
            return count > 0 ? elements[count - 1] : 0; 
            //if(count > 0)
            //    return elements[count - 1];
            //return 0; 
        }

        public int Pop()
        {
            return count > 0 ? elements[--count] : 0; 
            //if(count > 0)
            //{
            //    return elements[--count]; 
            //}
            //return 0; 
        }
    }
}
