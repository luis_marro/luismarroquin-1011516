﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ejercicio_1
{
    /// <summary>
    /// This class is used to represent each athlete in the olimpics 
    /// </summary>
    class Athlete
    {
        public string FirsName { get; set; }
        public string LastName { get; set; }
        public string Discipline { get; set; }
        public string LastMedal { get; set; }

        /// <summary>
        /// This mehtod will be used when an athlete wins a medal 
        /// </summary>
        /// <param name="medal"></param>
        public void win(string medal)
        {
            LastMedal = medal; 
        }
        
    }
}
