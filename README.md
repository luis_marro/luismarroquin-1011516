# README #

#Nombre: 
* Luis Marroquin 

#Usuario Bitbucket 
* luis_marro 

|         Curso            |    Nota   |
|--------------------------:|:-----------|
|Matemática I             |       81     | 
|Química 1                  |       89    |
|Estrategias de Razonamiento |   83  | 
|Estrategias de Comunicación Lingüistica|        93   | 
|Introducción a la Ingeniería en Informática y Sistemas |  95   | 
|Introducción a la Programación |  81    | 
|Matemática Discreta       |     74      | 
|Matemática II            |    71      |    